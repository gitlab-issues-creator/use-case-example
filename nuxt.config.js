import colors from 'vuetify/es5/util/colors'

export default {
  telemetry: false,
  ssr: true,
  globalName: 'gic',
  rootDir: __dirname,
  env: {
    APP_URL: process.env.APP_URL,
  },
  publicRuntimeConfig: {
    APP_URL: process.env.APP_URL,
    axios: {
      browserBaseURL: process.env.APP_URL || 'http://localhost:8080',
    },
  },
  privateRuntimeConfig: {
    APP_URL: process.env.APP_URL,
    axios: {
      baseURL: process.env.APP_URL || 'http://localhost:8080',
    },
  },
  build: {
    publicPath: '/static/',
    devtools: process.env.NODE_ENV === 'development',
    extractCSS: process.env.NODE_ENV !== 'development',
    transpile: ['typed-vuex'],
  },
  head: {
    titleTemplate: '%s - Gitlab Issues Creator',
    title: 'Gitlab Issues Creator',
    link: [
      {
        rel: 'stylesheet',
        href: 'https://fonts.googleapis.com/css?family=Material+Icons',
      },
    ],
    meta: [
      { charset: 'utf-8' },
      {
        name: 'viewport',
        content: 'width=device-width, initial-scale=1',
      },
      {
        hid: 'description',
        name: 'description',
        content: '',
      },
    ],
  },
  loading: {
    height: '3px',
    color: '#1976d2',
  },
  server: {
    port: 8080,
    host: '0.0.0.0',
  },
  css: [],
  buildModules: [['@nuxt/typescript-build', {
    typeCheck: {
      typescript: {
        extensions: {
          vue: true,
        },
      },
    },
  }], 'nuxt-typed-vuex', ['@nuxtjs/vuetify', {
    icons: {
      iconfont: 'mdi',
    },
  }]],
  modules: [
    '@nuxtjs/axios',
    'cookie-universal-nuxt',
  ],
  vuetify: {
    theme: {
      dark: false,
      themes: {
        dark: {
          primary: colors.blue.darken2,
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3,
        },
      },
    },
  },
  dir: {
    assets: 'src/assets',
    layouts: 'src/layouts',
    middleware: 'src/middleware',
    pages: 'src/pages',
    static: 'src/static',
    store: 'src/store',
  },
  serverMiddleware: [
    {
      path: '/api',
      handler: '~/api/index.ts',
    },
  ],
}
