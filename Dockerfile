FROM node:16-alpine
WORKDIR /app

ARG APP_URL
ARG DB_PASSWORD
ARG GITLAB_APP_SECRET
ARG GITLAB_PERSONAL_TOKEN

ENV APP_URL=${APP_URL}
ENV DB_PASSWORD=${DB_PASSWORD}
ENV GITLAB_APP_SECRET=${GITLAB_APP_SECRET}
ENV GITLAB_PERSONAL_TOKEN=${GITLAB_PERSONAL_TOKEN}

COPY package.json package.json
COPY yarn.lock yarn.lock
RUN apk add --update python3 build-base git openssh-client
RUN yarn
RUN npm install pm2 -g
COPY . /app
RUN yarn build

EXPOSE 80
CMD ["pm2-runtime", "ecosystem.config.js", "--format", "--only", "app"]
