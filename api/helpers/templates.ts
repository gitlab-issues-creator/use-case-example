import { GICTemplateList } from 'gitlab-issues-creator';

export type IssuesTemplate = 'vulnerability' | 'issue' | 'featureRequest' | 'exampleWebsite' | 'other'

export const issuesTemplates: GICTemplateList<IssuesTemplate> = {
  vulnerability: {
    title: 'Vulnerability',
    description: 'If you want to report a critical bug. Created issue will be private',
    defaultText: `- [x] Creating this issue I have tried to look for duplicates and didn't find one
- [x] Creating this issue I assume bug does exist

## Issue description

Please describe your issue as detailed as possible`,
    prependTitle: 'Vulnerability: ',
    labelsToAssign: ['priority::critical', 'vulnerability'],
    weight: 5,
    confidential: true,
  },
  issue: {
    title: 'Issue',
    description: 'If you want to report a problem',
    defaultText: `- [x] Creating this issue I have tried to look for duplicates and didn't find one
- [x] Creating this issue I assume bug does exist
- [x] Creating this issue I understand it will be public and visible for everyone, unless I have set confidential checkbox

## Issue description

Please describe your issue as detailed as possible`,
    prependTitle: 'Issue: ',
    labelsToAssign: ['bug'],
  },
  featureRequest: {
    title: 'Feature Request',
    description: 'If you want to suggest a feature',
    defaultText: `- [x] Creating this feature request I have tried to look for duplicates and didn't find one
- [x] Creating this feature request I understand it will be public and visible for everyone

## Feature Request description

Please describe your feature request as detailed as possible`,
    prependTitle: 'Feature Request: ',
    labelsToAssign: ['feature-request', 'priority::low'],
    weight: 0,
  },
  exampleWebsite: {
    title: 'Example Website',
    description: 'If you want to leave feedback, bug report, vulnerability or question about this site',
    defaultText: `- [x] Creating this ticket I have tried to look for duplicates and didn't find one
- [x] Creating this ticket I understand it will be public and visible for everyone, unless I have set confidential checkbox

## Ticket description

Please describe your ticket as detailed as possible`,
    prependTitle: 'Example Website: ',
    labelsToAssign: ['website'],
    weight: 1,
  },
  other: {
    title: 'Other',
    description: 'If you have question which is not a bug or feature request',
    defaultText: `- [x] Creating this ticket I have tried to look for duplicates and didn't find one
- [x] Creating this ticket I understand it will be public and visible for everyone

## Ticket description

Please describe your ticket as detailed as possible`,
    prependTitle: 'Question: ',
    labelsToAssign: ['question', 'priority::low'],
    weight: 1,
  },
};
