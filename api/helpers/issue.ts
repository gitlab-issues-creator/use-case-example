import IssueModel from './../models/issue.model';
import { IIssue } from '~/src/types';

export function createIssueFromModel(issue: IssueModel): IIssue {
  return {
    id: issue.id,
    title: issue.title,
    isClosed: issue.isClosed,
    issueIid: issue.issueIid,
    projectId: issue.projectId,
    url: issue.url,
    template: issue.template,
    priority: issue.priority,
    confidential: issue.confidential,
    createdAt: issue.createdAt,
  };
}
