import GICTemplate from 'gitlab-issues-creator';
import { issuesTemplates } from './templates';

export const APP_REDIRECT_URL = 'https://gitlab.dk4000.pro/api/gitlab/callback';
export const APP_URL = process.env.APP_URL || 'https://gitlab.dk4000.pro';
export const APP_SECRET = process.env.GITLAB_APP_SECRET || '';
export const APP_ID = 'a4006df87c820f88adbcc5a3217aa0bdd98c824eac35e796c688e95e5106cb56';
export const APP_TOKEN = process.env.GITLAB_PERSONAL_TOKEN || '';
export const APP_MAIN_PROJECT_ID = 22161981;
export const APP_TEST_PROJECT_ID = 22162284;

export const gic = new GICTemplate({
  personalAccessToken: APP_TOKEN,
  labels: ['approved'],
  projects: [APP_TEST_PROJECT_ID, APP_MAIN_PROJECT_ID],
  defaultAssigneesIds: [2537151],
  closeMessage: (projectId: number) => {
    const project: 'main' | 'test' = projectId === APP_MAIN_PROJECT_ID ? 'main' : 'test';
    const link = `https://gitlab.dk4000.pro/project/${ project }`;
    return `Your issue has been closed. Please report your issue on [dedicated form](${ link }) using a template.`;
  },
  templates: issuesTemplates,
  debug: {
    enabled: true,
    logLevels: 'all',
  },
});

export const gicOAuth = gic.oAuth({
  clientId: APP_ID,
  clientSecret: APP_SECRET,
  redirectUrl: APP_REDIRECT_URL,
  authScopes: ['api'],
});
