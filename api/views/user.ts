import { Request, Response } from 'express';
import TokenModel from '../models/token.model';
import UserModel from '../models/user.model';
import { IUser } from '~/src/types';
import IssueModel from '../models/issue.model';
import { randomBytes } from 'crypto';
import { createIssueFromModel } from './../helpers/issue';

export async function getUserInfo(req: Request, res: Response) {
  const header = req.headers.authorization;
  const token = typeof header === 'string' ? header.replace('Bearer ', '') : null;

  if (!token) return res.status(401).json({ error: 'Invalid request' });

  const response = await TokenModel.findOne({
    where: { token }, include: [
      {
        model: UserModel,
        include: [IssueModel],
      },
    ],
  });

  if (response?.expires && !response.user) return res.json({ anon: true, user: null });
  if (!response?.user) return res.status(401).json({ error: 'Invalid token' });

  const { user } = response;

  const iUser: IUser = {
    id: user.id,
    role: user.role,
    preferredLanguage: user.preferredLanguage,
    gitlab: {
      id: user.gitlabUserId,
      name: user.gitlabUserName,
      nickname: user.gitlabUserNickname,
      url: user.gitlabUserUrl,
      avatar: user.gitlabUserAvatar,
    },
    issues: user.issues
                .slice()
                .sort((a, b) => b.updatedAt.getTime() - a.updatedAt.getTime())
                .map((issue) => createIssueFromModel(issue)),
  };

  res.json({
    user: iUser,
    anon: false,
  });
}

export async function createAnonToken(req: Request, res: Response) {
  const token = randomBytes(24).toString('hex');
  await TokenModel.create({
    token,
    expires: new Date(Date.now() + 1000 * 60 * 60 * 24),
  });

  res.json({
    token,
  });
}
