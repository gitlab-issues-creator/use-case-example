import { Request, Response } from 'express';
import { issuesTemplates } from '../helpers/templates';
import { APP_TEST_PROJECT_ID, gic } from '../helpers/gitlab';
import { readFileSync } from 'fs';
import IssueModel from '../models/issue.model';
import { GICDebug } from 'gitlab-issues-creator/dist/classes/debug.class';
import { createIssueFromModel } from './../helpers/issue';
import { GICTemplateCreationInfo } from 'gitlab-issues-creator/dist/types/templates';

export async function getTemplates(req: Request, res: Response) {
  res.json({
    templates: gic.templatesWithIds,
  });
}

export async function createIssue(req: Request, res: Response) {
  try {
    const template = req.body.template;

    if (!Object.keys(issuesTemplates).includes(template)) return res.status(400).json({ error: 'Invalid issue type' });
    if (typeof req.body.title !== 'string') return res.status(400).json({ error: 'Invalid text' });
    if (typeof req.body.text !== 'string') return res.status(400).json({ error: 'Invalid text' });

    let priority: string | undefined;
    let numberPriority: 1 | 2 | 3 | 4 | null = null;
    let confidential = false;
    const projectId = +req.body.projectId;
    if(!req.user?.id && projectId !== APP_TEST_PROJECT_ID) return res.status(403).json({ error: 'You must be logged in as Gitlab user to create issues in this project' });

    if ((template === 'issue' || template === 'exampleWebsite') && req.body.priority) {
      switch (req.body.priority) {
        case '1':
          priority = 'priority::low';
          numberPriority = 1;
          break;
        case '2':
          priority = 'priority::medium';
          numberPriority = 2;
          break;
        case '3':
          priority = 'priority::high';
          numberPriority = 3;
          confidential = req.body.confidential === '1';
          break;
        case '4':
          priority = 'priority::critical';
          numberPriority = 4;
          confidential = req.body.confidential === '1';
          break;
      }
    }

    const labels: string[] = [];
    if (priority) labels.push(priority);

    const creationSettings: GICTemplateCreationInfo = {
      labelsToAssign: labels,
    };

    if (confidential) creationSettings.confidential = confidential;

    const issue = await gic.createIssue({
      template,
      title: req.body.title,
      description: req.body.text,
      oAuthAccessToken: req.user?.accessToken,
      projectId,
      files: Object.values(req.files).map(x => readFileSync(x.path)),
      creationSettings,
    });

    const issueModel = await IssueModel.create({
      title: issue.title,
      isClosed: false,
      issueIid: issue.iid,
      projectId: issue.project_id,
      url: issue.web_url,
      template,
      priority: numberPriority,
      userId: req.user?.id,
      confidential: issue.confidential,
    });

    res.status(201).json({
      issue: createIssueFromModel(issueModel),
    });
  }
  catch (e) {
    console.error(e);
    res.status(500).json({
      error: 'Unknown error occured',
    });
    new GICDebug(true).processError(e, 'issues-debug-frontend');
  }
}
