import { Request, Response } from 'express';
import { APP_URL, gic, gicOAuth } from '../helpers/gitlab';
import UserModel from '../models/user.model';
import { randomBytes } from 'crypto';
import TokenModel from '../models/token.model';
import IssueModel from './../models/issue.model';

export async function gitlabAuthRedirect(req: Request, res: Response) {
  const state = req.query.state;
  res.redirect(gicOAuth.getRedirectURL({ state: typeof state === 'string' ? state : undefined }));
}

export async function gitlabAuthCallback(req: Request, res: Response) {
  const { code } = req.query;
  if (typeof code !== 'string') return res.redirect(`${ APP_URL }/?error=gitlab`);

  try {
    const callbackResult = await gicOAuth.processAuthCallback({ code });
    if (!callbackResult) return res.redirect(`${ APP_URL }/?error=token-invalid`);

    const { user, tokenResponse } = callbackResult;

    const userModel = await UserModel.findOne({ where: { gitlabUserId: user.id } }) || UserModel.build({
      gitlabUserId: user.id,
    });

    userModel.role = user.username === 'daniluk4000' ? 'admin' : 'user';
    userModel.accessToken = tokenResponse.access_token;
    userModel.accessTokenExpiresIn = tokenResponse.expires_in
      ? new Date(Date.now() + 1000 * tokenResponse.expires_in)
      : null;
    userModel.refreshToken = tokenResponse.refresh_token;
    userModel.gitlabUserId = user.id;
    userModel.gitlabUserNickname = user.username;
    userModel.gitlabUserName = user.name;
    userModel.gitlabUserAvatar = user.avatar_url || null;
    userModel.gitlabUserUrl = user.web_url;
    await userModel.save();

    const token = randomBytes(24).toString('hex');
    await TokenModel.create({
      userId: userModel.id,
      token,
    });

    return res.redirect(`${ APP_URL }${ req.query.state || '/' }?token=${ encodeURIComponent(token) }`);
  }
  catch (e) {
    console.error(e);

    res.redirect(`${ APP_URL }/?error=token-error`);
  }
}

export async function gitlabEventWebhook(req: Request, res: Response) {
  res.send('ok');

  if (!req.body) return;

  const webhook = await gic.processWebhook(req.body);
  if (!webhook) return;

  if (webhook.event === 'issue-closed') {
    await IssueModel.update({
        isClosed: true,
      },
      {
        where: {
          issueIid: webhook.issue.object_attributes.iid,
          projectId: webhook.issue.object_attributes.project_id,
        },
        returning: false,
      });
  }

  if (webhook.event === 'issue-unknown') {
    await IssueModel.update({
        isClosed: webhook.isClosed,
      },
      {
        where: {
          issueIid: webhook.issue.object_attributes.iid,
          projectId: webhook.issue.object_attributes.project_id,
        },
        returning: false,
      });
  }
}
