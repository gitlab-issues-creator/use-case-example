import { Sequelize } from 'sequelize-typescript';
import IssueModel from "./models/issue.model";
import TokenModel from "./models/token.model";
import UserModel from "./models/user.model";

export const db = new Sequelize({
  host: 'fb7915xs.beget.tech',
  database: 'fb7915xs_gic',
  username: 'fb7915xs_gic',
  password: process.env.DB_PASSWORD,
  dialect: 'mysql',
  models: [IssueModel, TokenModel, UserModel],
  logging: false,
});
