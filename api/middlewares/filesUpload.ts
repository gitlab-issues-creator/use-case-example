import * as Formidable from 'formidable';
import { File } from 'formidable';
import { NextFunction, Request, Response } from 'express';
import UserModel from '../models/user.model';

export async function filesUpload(req: Request, res: Response, next: NextFunction) {
  const form = new Formidable.IncomingForm();

  let parsed = false;

  setTimeout(() => {
    if (!parsed) {
      res.status(400).json({
        status: 0,
        error: 'No files received',
      });
      return;
    }
  }, 3000);

  form.parse(req, async (form, fields, files) => {
    parsed = true;
    req.body = fields;

    const uploadFiles: {
      [key: string]: File;
    } = {};

    Object.entries(files).forEach(([key, file]) => {
      uploadFiles[key] = Array.isArray(file) ? file[0] : file;
    });

    req.files = uploadFiles;
    next();
  });
}

declare global {
  namespace Express {
    interface Request {
      files: Record<string, File>
      user?: UserModel
    }
  }
}
