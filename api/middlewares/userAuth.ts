import * as express from 'express';
import TokenModel from '../models/token.model';
import UserModel from '../models/user.model';

export async function userAuth(req: express.Request, res: express.Response, next: express.NextFunction) {
  try {
    if (req.headers.authorization) {
      const auth = req.headers.authorization.replace('Bearer ', '');

      const token = await TokenModel.findOne({ attributes: ['userId'], where: { token: auth } });
      if (token?.expires && token.expires.getTime() < Date.now()) {
        await token.destroy();
        return res.status(401).json({
          status: 0,
          error: 'Unauthorized',
        });
      }

      const user = await UserModel.findByPk(token?.userId);

      if (user) {
        req.user = user;
        next();
        return;
      }
      else if (token) {
        next();
        return;
      }
    }
  }
  catch (e) {
    console.error(e);
  }

  res.status(401).json({
    status: 0,
    error: 'Unauthorized',
  });
}
