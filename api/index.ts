import express from 'express';
import bodyParser from 'body-parser';
import { db } from './db';
import dotenv from 'dotenv';
import { gitlabAuthCallback, gitlabAuthRedirect, gitlabEventWebhook } from './views/gitlab';
import { createAnonToken, getUserInfo } from './views/user';
import { userAuth } from './middlewares/userAuth';
import { filesUpload } from './middlewares/filesUpload';
import { createIssue, getTemplates } from './views/issues';
import { existsSync } from 'fs';

const app = express();
app.use(bodyParser.json());

const envPath = `${ __dirname }/../.env`;
if (existsSync(envPath)) dotenv.config({
  path: `${ __dirname }/../.env`,
});

app.get('/gitlab/auth', gitlabAuthRedirect);
app.get('/gitlab/callback', gitlabAuthCallback);
app.all('/gitlab/callback/event', gitlabEventWebhook);

app.get('/user', userAuth, getUserInfo);
app.post('/user/anon', createAnonToken);
app.post('/issues', userAuth, filesUpload, createIssue);
app.get('/issues/templates', userAuth, getTemplates);

!async function () {
  await db.authenticate();
  await db.sync();
  console.log('DB Connection has been established');
}();

module.exports = app;
