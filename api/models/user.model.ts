import {
  AllowNull,
  AutoIncrement,
  Column,
  DataType,
  Default,
  HasMany,
  Index,
  Model,
  PrimaryKey,
  Table,
  Unique,
} from 'sequelize-typescript';
import IssueModel from './issue.model';
import TokenModel from './token.model';
import { IUser } from './../../src/types';

@Table({
  modelName: 'user',
})
export default class UserModel extends Model<UserModel> {
  @AutoIncrement
  @PrimaryKey
  @Column(DataType.INTEGER)
  id: number;

  @AllowNull(false)
  @Default('user')
  @Column(DataType.STRING)
  role: IUser['role'];

  @AllowNull(false)
  @Column(DataType.STRING)
  accessToken: string;

  @Index('access-token-expires-index')
  @Column('TIMESTAMP')
  accessTokenExpiresIn: Date | null;

  @AllowNull(false)
  @Column(DataType.STRING)
  refreshToken: string;

  @Default('en')
  @AllowNull(false)
  @Column(DataType.STRING)
  preferredLanguage: IUser['preferredLanguage'];

  @Unique
  @AllowNull(false)
  @Column(DataType.INTEGER)
  gitlabUserId: number;

  @AllowNull(false)
  @Column(DataType.STRING)
  gitlabUserNickname: string;

  @AllowNull(false)
  @Column(DataType.STRING)
  gitlabUserName: string;

  @Column(DataType.STRING)
  gitlabUserAvatar: string | null;

  @AllowNull(false)
  @Column(DataType.STRING)
  gitlabUserUrl: string;

  @HasMany(() => IssueModel, { onDelete: 'CASCADE' })
  issues: IssueModel[];

  @HasMany(() => TokenModel, { onDelete: 'CASCADE' })
  tokens: TokenModel[];
}
