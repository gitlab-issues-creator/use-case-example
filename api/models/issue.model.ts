import {
  AllowNull,
  AutoIncrement,
  BelongsTo,
  Column,
  createIndexDecorator,
  DataType,
  Default,
  ForeignKey,
  Index,
  Model,
  PrimaryKey,
  Table,
} from 'sequelize-typescript';
import UserModel from './user.model';
import { IssuesTemplate } from '../helpers/templates';

const ProjectIdIssueIidIndex = createIndexDecorator({
  name: 'project-issue-ids-index',
  unique: true,
});

@Table({
  modelName: 'issue',
})
export default class IssueModel extends Model<IssueModel> {
  @AutoIncrement
  @PrimaryKey
  @Column(DataType.INTEGER)
  id: number;

  @AllowNull(false)
  @Column(DataType.STRING)
  title: string;

  @AllowNull(false)
  @Default(false)
  @Column(DataType.BOOLEAN)
  isClosed: boolean;

  @ProjectIdIssueIidIndex
  @AllowNull(false)
  @Column(DataType.INTEGER)
  issueIid: number;

  @ProjectIdIssueIidIndex
  @AllowNull(false)
  @Column(DataType.INTEGER)
  projectId: number;

  @AllowNull(false)
  @Column(DataType.STRING)
  url: string;

  @AllowNull(false)
  @Column(DataType.STRING)
  template: IssuesTemplate;

  @Column(DataType.INTEGER)
  priority: 1 | 2 | 3 | 4 | null;

  @AllowNull(false)
  @Column(DataType.BOOLEAN)
  confidential: boolean;

  @Index('user-id-index')
  @ForeignKey(() => UserModel)
  @Column(DataType.INTEGER)
  userId: number;

  @BelongsTo(() => UserModel, { onDelete: 'CASCADE' })
  user: UserModel;
}
