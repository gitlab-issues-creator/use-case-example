import {
  AllowNull,
  AutoIncrement,
  BelongsTo,
  Column,
  DataType,
  ForeignKey,
  Index,
  Model,
  PrimaryKey,
  Table,
  Unique,
} from 'sequelize-typescript';
import UserModel from './user.model';

@Table({
  modelName: 'token',
})
export default class TokenModel extends Model<TokenModel> {
  @AutoIncrement
  @PrimaryKey
  @Column(DataType.INTEGER)
  id: number;

  @Unique
  @Index('token-index')
  @AllowNull(false)
  @Column(DataType.STRING)
  token: string;

  @Column('TIMESTAMP')
  expires: Date;

  @ForeignKey(() => UserModel)
  @Column(DataType.INTEGER)
  userId: number;

  @BelongsTo(() => UserModel, { onDelete: 'CASCADE' })
  user: UserModel;
}
