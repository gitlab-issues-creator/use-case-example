import { actionTree, getAccessorType, getterTree, mutationTree } from 'typed-vuex';
import { IUser } from '~/src/types';
import { Context } from '@nuxt/types';
import { GICTemplateFrontendList } from 'gitlab-issues-creator/dist/types/templates';
import { issuesTemplates } from '~/api/helpers/templates';

export const strict = false;

export const state = () => ({
  user: null as IUser | null,
  token: '' as string,
  anon: false,
  templates: [] as GICTemplateFrontendList<typeof issuesTemplates>,
});

export const getters = getterTree(state, {});

export const mutations = mutationTree(state, {
  SET_USER(state, user: IUser) {
    state.user = user;
  },
  SET_TOKEN(state, token: string) {
    state.token = token;
  },
  SET_ANON(state, anon: boolean) {
    state.anon = anon;
  },
  SET_TEMPLATES(state, templates: GICTemplateFrontendList<typeof issuesTemplates>) {
    state.templates = templates;
  },
});

export const actions = actionTree(
  { state, getters, mutations },
  {
    async nuxtServerInit({ commit, dispatch }, { query, redirect }: Context) {
      const token = query.token || this.app.$cookies.get('gic-token');
      if (typeof token !== 'string') return;

      try {
        const { user, anon } = (await this.app.$axios.get<{ user: IUser | null, anon: boolean }>('/api/user', {
          headers: {
            authorization: `Bearer ${ token }`,
          },
          responseType: 'json',
        })).data;

        if (query.token) {
          this.app.$cookies.remove('gic-token');
          this.app.$cookies.set('gic-token', token, {
            path: '/',
            maxAge: 60 * 60 * 24 * 7,
            sameSite: true,
          });
        }

        if (anon) commit('SET_ANON', true);
        else if (user) commit('SET_USER', user);

        commit('SET_TOKEN', token);

        if (anon || user) {
          await dispatch('updateTemplates');
        }
      }
      catch (e) {

      }
    },
    async updateTemplates({ state, commit }) {
      commit('SET_TEMPLATES', (await this.$axios.get('/api/issues/templates', {
        headers: { authorization: `Bearer ${ state.token }` },
        responseType: 'json',
      })).data.templates);
    },
  },
);

export const accessorType = getAccessorType({
  state,
  getters,
  mutations,
  actions,
});
