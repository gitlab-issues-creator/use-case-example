import { accessorType } from '~/src/store';
import { Cookies } from '~/src/types/index';

declare module 'vue/types/vue' {
  interface Vue {
    $accessor: typeof accessorType
    $cookies: Cookies
  }
}

declare module '@nuxt/types' {
  interface NuxtAppOptions {
    $accessor: typeof accessorType
    $cookies: Cookies
  }
}
