import { issuesTemplates } from '../../api/helpers/templates';
import { GICTemplateFrontendList } from 'gitlab-issues-creator/dist/types/templates';

export interface IUser {
  id: number
  role: 'user' | 'admin'
  preferredLanguage: 'ru' | 'en'
  gitlab: {
    id: number
    nickname: string
    name: string
    avatar: string | null
    url: string
  }
  issues: IIssue[]
}

export interface IIssue {
  id: number
  title: string
  isClosed: boolean
  issueIid: number
  projectId: number
  url: string
  template: keyof typeof issuesTemplates
  priority: 1 | 2 | 3 | 4 | null
  confidential: boolean
  createdAt: string
}

export type ITemplate = GICTemplateFrontendList<typeof issuesTemplates>[0]

export interface Cookies {
  get(cookie: string): string | null

  remove(cookie: string): void

  set(cookie: string, value: string, options: CookiesSetOptions): void
}

export interface CookiesSetOptions {
  path: '/',
  expires?: Date,
  maxAge: number
  httpOnly?: boolean
  domain?: string
  encode?: Function
  sameSite?: string | boolean
  secure?: boolean
}
