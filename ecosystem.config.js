module.exports = {
  apps: [{
    name: 'app',
    script: 'npm',
    args: 'start',
    time: true,
  }],
}
